import React from 'react';
import "./Navigation.css";
import TopHeader from "./TopHeader";
import BottomHeaderAfterCustomization from "./BottomHeader_AfterCustomization";

function Navigation() {
    return (
        <div className="navigation">
            <TopHeader/>
            <BottomHeaderAfterCustomization/>
        </div>
    )
}

export default Navigation
