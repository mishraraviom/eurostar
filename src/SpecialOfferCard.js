import React from 'react'
import "./SpecialOfferCard.css"

function SpecialOfferCard(props) {
    return (
        <div className="SpecialOfferCard">
            <div className="specialOffer-image">
                <img src={props.img} alt="" className="SI" />
                <div className="specialOffer-info-part">
                    <div className="off-percentage">
                        <p className="offPercentage-p">off</p>
                        <h4 className="offPercentage-h">{props.perc}</h4>
                    </div>
                    <div className="info-detail">
                        <h5 className="info-detail-parts">{props.explaining}</h5>
                        <p className="info-detail-parts"><span className="info-detail-Span">Event Validity :</span>{props.EventValidity}</p>
                        <p className="info-detail-parts info-detail-lastPara">{props.Before}</p>
                        <button className="callNow-button">Call Now</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SpecialOfferCard
