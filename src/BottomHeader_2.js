import React,{useState} from 'react';
import "./BottomHeader_2.css"
import MenuIcon from '@material-ui/icons/Menu';
import {Link} from "react-router-dom";

function BottomHeader_2() {
    const [imageClick,setImageClick]=useState(false);
    
    function handleOnClickProperty(){
        setImageClick(prevValue=>{
            return !prevValue;
        });
    }



    return (
        <div className="bottomheader_2">
            <Link to="/">
                <div className="bottomheader_2_image">
                <img src={require("./images/esi_logo.gif")}  alt="" />
                </div>
            </Link>
            <div className="bottomheader_2_Detail">
                <div onClick={handleOnClickProperty}>
                    <MenuIcon className="bottomheader_menuicon"   />
                    <div id="dropdownContentList" className={imageClick ? "dropdownContent show" : "dropdownContent"} >
                    <Link to="/" className="a" style={{textDecoration :"none"}} >Home</Link>
                    <Link to="/Accomodation" className="a" style={{textDecoration:"none"}}>Accomodation</Link>
    
                        <Link to="/Booking" style={{textDecoration:"none"}} className="a bleft">Booking</Link>
                        <Link to="/HotelFacility" style={{textDecoration:"none"}} className="a bleft"> Hotel Facility </Link>
                        <Link to="PhotoGallery" style={{textDecoration:"none"}} className="a bleft">Photo Gallery</Link>
                    
                    <Link to="/SpecialOffer" style={{textDecoration:"none"}} className="a">Special Offers</Link>                   
                    <Link to="/" className="a" style={{textDecoration:"none"}}>Review Us</Link>
                    <Link to="ContactUs" style={{textDecoration:"none"}} className="a">Contact Us</Link>
                </div>
                </div>
                <div className="sideline_bottomheader_nav"></div>
                <div className="bottomheader_2_heading">
                    <h4>Navigation</h4>
                </div>
            </div>
            
        </div>
    )
}

export default BottomHeader_2
