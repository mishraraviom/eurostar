import React from 'react'
import "./Accomodation.css";
import {Link} from "react-router-dom";

function AccomodationCard(props) {
    console.log(props.occupancy);
    return (
        <div className="accomodation">
            
            <div className="accomodation_card">
                <img src={props.img} alt="" className="accomodation-image" />
                <div className="accomodation_info">
                    <h1>{props.heading}</h1>
                </div>
                <div className="accomodation-red-line"></div>
                <div className="info-detail">
                    <p><span className="Span">Beds :</span> {props.Bedsize}</p>
                    <p><span className="Span">Occupancy :</span>{props.occupancy}</p>
                    <p><span className="Span">In Room Balcony :</span>{props.Balcony}</p>
                    <p><span className="Span">Air Condition :</span>{props.AC}</p>
                </div>
                <div className="accomodation-buttons">
                    <Link to={props.link1}><button className="roomDetail">Rooms Detail</button></Link>
                    <Link to={props.link2}><button className="checkAvailability">Check Availability</button></Link>
                </div>
            </div>
        </div>

    )
}

export default AccomodationCard;


