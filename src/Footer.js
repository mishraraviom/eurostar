import React from 'react'
import "./Footer.css";
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import {Link} from "react-router-dom";

function Footer() {
    return (
        <div className="footer_special">
            <div className="footer">
                <div className="center_of_excellence">
                    <h5>Certificate of Excellence</h5>
                    <div className="bottomLine"></div>
                    <div className="COE_image">
                        <img src={require("./images/download.png")} alt="" />
                    </div>
                </div>
                <div className="Bravo_award">
                    <h5>Bravo Award</h5>
                    <div className="bottomLine"></div>
                    <div className="bravo_image">
                        <img src={require("./images/download.png")} alt="" />
                    </div>
                </div>
                <div className="social_media">
                    <h5>Social Media</h5>
                    <div className="bottomLine"></div>
                    <div className="social_media_icons">
                        <InstagramIcon />
                        <br></br>
                        <FacebookIcon />
                    </div>
                </div>
                <div className="siteMap">
                    <h5>Site Map</h5>
                    <div className="bottomLine"></div>
                    <div >
                    <ul className="site_map_links">
                        <li><Link to="/Accomodation" style={{textDecoration:"none" , color:"#fff"}}>Accomodation</Link></li>
                        <li><Link to="/SpecialOffer" style={{textDecoration:"none" , color:"#fff"}}>Special Offers</Link></li>
                        <li><Link to="/ContactUs" style={{textDecoration:"none",color:"#fff"}}>Contact Us</Link></li>
                    </ul>
                        
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default Footer
