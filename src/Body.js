import React from 'react'
import "./Body.css";

function Body() {
    return (
        <div className="body">
            <div className="about_erurostar">
                <h3 className="title-style1">About EuroStarInn</h3>
                <div className="title-block"></div>
                <img src={require("./images/demo_image_01.jpg")}  className="response-image image" alt="" />
                <div className="about_detail">
                    <p>EuroStar Inn was created to offer maximum comfort to its clients. The big rooms are furnished and decorated in a minimal style which allows the perfect space use. In addition, it is decorated with warm colors which create a great welcome atmosphere for its clients.</p>
                </div>
                <button className="about_button">Read more</button>
            </div>
            <div className="hotel_overview">
                <h3 className="title-style1">Hotel Overview</h3>
                    <div className="title-block"></div>
                <div className="overview_list">
                    <ol>
                        <li>24 Hours Room Service</li>
                        <li>Air Conditioned Rooms</li>
                        <li>Cable LCD T.V</li>
                        <li>Roof top/Ac Restaurant,With room service</li>
                        <li>Airport/Taxi assistance facility</li>
                        <li>Morning yoga classes and massage facility</li>
                        <li>Money changer</li>
                        <li>Wake up call services, fax and Xerox</li>
                        <li>Conference room, reception 24/7, free parking</li>
                     </ol>
                </div>
                <button className="overview_button">Read more</button>
            </div>
            <div className="location">
                <h3 className="title-style1">Location</h3>
                <div className="title-block"></div>
                <div className="location_detail">
                    <p>EuroStar Inn was created to offer maximum comfort to its clients. The big rooms are furnished and decorated in a minimal style which allows the perfect space use. In addition, it is decorated with warm colors which create a great welcome atmosphere for its clients.</p>
                </div>
            </div>
        </div>
    )
}

export default Body
