import React,{useState} from 'react'
import "./HotelFacility.css";
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import SubjectIcon from '@material-ui/icons/Subject';
import HomeIcon from '@material-ui/icons/Home';
import CheckInCalender from "react-calendar";
import CheckOutCalender from "react-calendar";
import 'react-calendar/dist/Calendar.css';

function HotelFacility() {

    // /////////////////////////////

    const [checkingIn,setCheckingIn] = useState(false);
    const [checkingOut,setCheckingOut] = useState(false);
    const [getCheckInDate,setCheckInDate] =useState(checkingIn ? new Date() : null);
    const [getCheckOutDate,setCheckOutDate]=useState(checkingOut ? new Date() : null);


    function handelGetCheckingIn(ranges){
        setCheckInDate(ranges.toLocaleDateString());
    }

    function handleGetCheckingOut(ranges){
        setCheckOutDate(ranges.toLocaleDateString());
    }

    function handleCheckInOnClick(){
        setCheckingIn(prevValue=>{
            return !prevValue;
        });
    }

    function handleCheckOutOnClick(){
        setCheckingOut(prevValue=>{
            return !prevValue;
        });
    }

    //////////////////////////////

    function handleHotelForm(event){
        event.preventDefault();
    }

    return (
        <div>
            <div className="hotelfacility_header"></div>
            <h1 className="hotelfacility_h1">Hotel Facility</h1>
            <div className="hotelfacility_bodypart">
                <div className="hotelfacility_portion">
                    <ul className="hotelfacility_lists">
                        <li>Choice of elegantly furnished air-conditioned and air-cooled rooms.</li>
                        <li>Wifi Facility available.</li>
                        <li>Attached bath with running hot and cold water.</li>
                        <li>Telephone and LCD TV with cable in each room.</li>
                        <li>24-hours room service.</li>
                        <li>Laundry and dry cleaning service.</li>
                        <li>Doctor on call.</li>
                        <li>Ample car parking facility.</li>
                        <li>Airport transfers.</li>
                        <li>Tour and Travels Information.</li>
                        <li>Rent A Car</li>
                        <li>International & Domestic ticketing</li>
                        <li>Package tours</li>
                        <li>Foreign exchange facility</li>
                        <li>Reconfirmation of tickets</li>
                        <li>Hotel booking throughout India</li>
                        <li>Cable LCD T.V</li>
                        <li>Airport/Taxi assistance facility</li>
                        <li>Morning yoga classes and massage facility</li>
                        <li>Wake up call services, fax and Xerox</li>
                         <li>Conference room, reception 24/7, free parking</li>
                    </ul>
                </div>
                    {/* ----------------------- */}
                    <div className="contactDetail">
                        <h3>Reservation</h3>
                        <div className="reservationbottomline"></div>
                        <div className="container">
                                <form action="/action_page.php" onSubmit={handleHotelForm}>
                                <div className="row">
                                    <div className="col-75">
                                         <div>{ checkingIn ? <CheckInCalender onChange={handelGetCheckingIn} className="CheckInCalenderHotelFacility" /> : null}</div>
                                        <input type="text" value={getCheckInDate} className="firstInput" id="fname" name="firstname" placeholder="Check In" onClick={handleCheckInOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <div>{ checkingOut ? <CheckOutCalender  onChange={handleGetCheckingOut} className="CheckOutCalenderHotelFacility" /> : null  }</div>
                                        <input type="text" value={getCheckOutDate} id="lname" className="secondInput" name="lastname" placeholder="Check Out"  onClick={handleCheckOutOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="firstSelect">
                                        <option value="Rooms">Rooms</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="secondSelect">
                                        <option value="persons">Persons</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <input type="submit" name="submit" className="submittypebutton" value="Check Availability" />
                                </div>
                            </form>
                        </div>
                            <div className="silverLine"></div>
                        {/* ----------------------- */}
                        <div className="ContactForm">
                            <h3>Contact Detail</h3>
                            <div className="reservationbottomline"></div>
                            <div className="makingDown">
                                <div className="contact">
                                    <PhoneIcon />
                                    <p className="setp">+21345612345</p>
                                </div>
                                <div className="contact">
                                    <SubjectIcon />
                                    <p className="setp">123454234</p>
                                </div>
                                <div className="contact">
                                    <EmailIcon />
                                    <p className="setp">contactus@eurostarinn.com</p>
                                </div>
                                <div className="contact">
                                    <HomeIcon />
                                    <p className="setp">Khajuraho</p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default HotelFacility
