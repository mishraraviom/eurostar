import React ,{useState} from 'react'
import "./SpecialOffer.css"
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import SubjectIcon from '@material-ui/icons/Subject';
import HomeIcon from '@material-ui/icons/Home';
import CheckInCalender from "react-calendar";
import CheckOutCalender from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import SpecialOfferCard from "./SpecialOfferCard";

function SpecialOffer() {

    // /////////////////////////////

    const [checkingIn,setCheckingIn] = useState(false);
    const [checkingOut,setCheckingOut] = useState(false);
    const [getCheckInDate,setCheckInDate] =useState(checkingIn ? new Date() : null);
    const [getCheckOutDate,setCheckOutDate]=useState(checkingOut ? new Date() : null);


    function handelGetCheckingIn(ranges){
        setCheckInDate(ranges.toLocaleDateString());
    }

    function handleGetCheckingOut(ranges){
        setCheckOutDate(ranges.toLocaleDateString());
    }

    function handleCheckInOnClick(){
        setCheckingIn(prevValue=>{
            return !prevValue;
        });
    }

    function handleCheckOutOnClick(){
        setCheckingOut(prevValue=>{
            return !prevValue;
        });
    }

    //////////////////////////////

    function handleHotelForm(event){
        event.preventDefault();
    }

    return (
        <div className="specialOffer">
            <div className="hotelfacility_header"></div>
            <h1 className="hotelfacility_h1">Special Offer</h1>
            <div className="hotelfacility_bodypart">
                <div className="hotelfacility_portion">
                   <SpecialOfferCard 
                   img={require("./images/SpecialOffers.jpg")} 
                   perc={"10%"} 
                   EventValidity={"October 15"} 
                   explaining={"SPECIAL SEASION OFFER- Double Deluxe Room"} 
                   Before={"Plan your travel before 15 October and enjoy our special discounted rate benefits upto 10%"}  
                   />

                   <SpecialOfferCard 
                   img={require("./images/GiftlastMinute.png")} 
                   perc={"30%"} 
                   EventValidity={"occupancy within 2 hours / validity upto : october 31"} 
                   explaining={"Last Minute Bid- Double Deluxe Room"} 
                   Before={"Are you in town and looking for immediate occupancy ? call use if we have room available you can get upto 30% discount."}  
                   />

                   <SpecialOfferCard 
                   img={require("./images/Book-in-Advance.jpg")} 
                   perc={"20%"} 
                   EventValidity={"November 30"} 
                   explaining={"BOOK EARLY & SAVE MORE- Double Deluxe Room"} 
                   Before={"It's smart to plan ahead why not extend your budget by booking early. Book 1 month early and get 20% discount"}  
                   />

                   <SpecialOfferCard 
                   img={require("./images/ducksinarow.jpg")} 
                   perc={"25%"} 
                   EventValidity={"October 15"} 
                   explaining={"STAY MORE & SAVE MORE- Double Deluxe Room"} 
                   Before={"Stay longer than 1 day and enjoy our special discounted rate benefits upto 25%"}  
                   />
                </div>
                    {/* ----------------------- */}
                    <div className="contactDetail">
                        <h3>Reservation</h3>
                        <div className="reservationbottomline"></div>
                        <div className="container">
                                <form action="/action_page.php" onSubmit={handleHotelForm}>
                                <div className="row">
                                    <div className="col-75">
                                         <div>{ checkingIn ? <CheckInCalender onChange={handelGetCheckingIn} className="CheckInCalenderHotelFacility" /> : null}</div>
                                        <input type="text" value={getCheckInDate} className="firstInput" id="fname" name="firstname" placeholder="Check In" onClick={handleCheckInOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <div>{ checkingOut ? <CheckOutCalender  onChange={handleGetCheckingOut} className="CheckOutCalenderHotelFacility" /> : null  }</div>
                                        <input type="text" value={getCheckOutDate} id="lname" className="secondInput" name="lastname" placeholder="Check Out"  onClick={handleCheckOutOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="firstSelect">
                                        <option value="Rooms">Rooms</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="secondSelect">
                                        <option value="persons">Persons</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <input type="submit" name="submit" className="submittypebutton" value="Check Availability" />
                                </div>
                            </form>
                        </div>
                            <div className="silverLine"></div>
                        {/* ----------------------- */}
                        <div className="ContactForm">
                            <h3>Contact Detail</h3>
                            <div className="reservationbottomline"></div>
                            <div className="makingDown">
                                <div className="contact">
                                    <PhoneIcon />
                                    <p className="setp">+21345612345</p>
                                </div>
                                <div className="contact">
                                    <SubjectIcon />
                                    <p className="setp">123454234</p>
                                </div>
                                <div className="contact">
                                    <EmailIcon />
                                    <p className="setp">contactus@eurostarinn.com</p>
                                </div>
                                <div className="contact">
                                    <HomeIcon />
                                    <p className="setp">Khajuraho</p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default SpecialOffer 
