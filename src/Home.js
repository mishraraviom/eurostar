import React from 'react';
import Carousal from "./Carousal"
import ChoosingBookingDate from "./ChoosingBookingDate";
import Body from "./Body";

function Home() {
    return (
        <div>
            <Carousal />
            <ChoosingBookingDate />
            <Body />
        </div>
    )
}

export default Home
