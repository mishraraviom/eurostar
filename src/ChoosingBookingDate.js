import React,{useState} from 'react'
import "./ChoosingBookingDate.css";
import CheckInCalender from "react-calendar";
import CheckOutCalender from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import {Link} from "react-router-dom";

function ChoosingBookingDate() {

    const [checkingIn,setCheckingIn] = useState(false);
    const [checkingOut,setCheckingOut] = useState(false);
    const [getCheckInDate,setCheckInDate] =useState(checkingIn ? new Date() : null);
    const [getCheckOutDate,setCheckOutDate]=useState(checkingOut ? new Date() : null);

    /////making datepicker //////

    function handelGetCheckingIn(ranges){
        setCheckInDate(ranges.toLocaleDateString());
    }

    function handleGetCheckingOut(ranges){
        setCheckOutDate(ranges.toLocaleDateString());
    }

    function handleCheckInOnClick(){
        setCheckingIn(prevValue=>{
            return !prevValue;
        });
    }

    function handleCheckOutOnClick(){
        setCheckingOut(prevValue=>{
            return !prevValue;
        });
    }


    ///////////////////////////////

    function handleCheckAvailability(event){
        event.preventDefault();
    }

    return (
        
        <div className="choosingbookingdate_upper_part">

            <div>{ checkingIn ? <CheckInCalender onChange={handelGetCheckingIn} className="CheckInCalender" /> : null}</div>
            <div>{ checkingOut ? <CheckOutCalender  onChange={handleGetCheckingOut} className="CheckOutCalender" /> : null  }</div>
            <form className="form-inline" onSubmit={handleCheckAvailability} >
                
                <input type="text" value={getCheckInDate} onClick={handleCheckInOnClick}  placeholder="Check In" name="checkin" className="first_input_check" />
                <input type="text" value={getCheckOutDate}  onClick={handleCheckOutOnClick}  placeholder="Check Out" name="checkout" className="second_input_check" />
                
                
                <select className="first_select">
                    <option value="rooms">Rooms</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select className="second_select">
                    <option value="persons">Persons</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <Link to="/Booking">
                <button type="submit" className="Submit" >Check Availability</button>
                </Link>
            </form>
        </div>
    )
}

export default ChoosingBookingDate
