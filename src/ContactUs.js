import React,{useState} from 'react'
import "./ContactUs.css";
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import SubjectIcon from '@material-ui/icons/Subject';
import HomeIcon from '@material-ui/icons/Home';
import CheckInCalender from "react-calendar";
import CheckOutCalender from "react-calendar";
import 'react-calendar/dist/Calendar.css';

function ContactUs() {

    // /////////////////////////////

    const [checkingIn,setCheckingIn] = useState(false);
    const [checkingOut,setCheckingOut] = useState(false);
    const [getCheckInDate,setCheckInDate] =useState(checkingIn ? new Date() : null);
    const [getCheckOutDate,setCheckOutDate]=useState(checkingOut ? new Date() : null);


    function handelGetCheckingIn(ranges){
        setCheckInDate(ranges.toLocaleDateString());
    }

    function handleGetCheckingOut(ranges){
        setCheckOutDate(ranges.toLocaleDateString());
    }

    function handleCheckInOnClick(){
        setCheckingIn(prevValue=>{
            return !prevValue;
        });
    }

    function handleCheckOutOnClick(){
        setCheckingOut(prevValue=>{
            return !prevValue;
        });
    }

    /////////////////////

    function handleHotelForm(event){
        event.preventDefault();
    }
    return (
        <div className="contactus">
            <div className="contactUpper_image"></div>
            <div className="contact_h3" ><h3>Contact Us</h3></div>

            {/* ----------------- */}

            <div className="hotelfacility_bodypart">
                <div className="hotelfacility_portion">
                   <div className="contactUs-emailus">
                        <h4>Email Us</h4>
                        
                   </div>
                   <div className="emailus-bootomline"></div>

                   <form className="contactUs-form" onSubmit={handleHotelForm}>
                        <div className="contact-label">
                            <label for="name">Name *</label>
                        </div>
                        <div className="input_margin">
                            <input type="text" className="contact_input" placeholder="Enter Your Name" />
                        </div>
                        <div className="contact-label">
                            <label for="email">Email</label>
                        </div>
                        <div className="input_margin">
                            <input type="text" className="contact_input" placeholder="Email" />
                        </div>
                        <div className="contact-label">
                            <label for="MobileNumber">Mobile Number </label>
                        </div>
                        <div className="input_margin">
                            <input type="text" className="contact_input" placeholder="Mobile Number" />
                        </div>
                        <div className="contact-label">
                            <label for="comment">Comment</label>
                        </div>
                        <div className="input_margin">
                            <textarea  className="text-area" rows="5" cols="40" />
                        </div>
                        <div className="input_margin_button">
                            <input type="submit" className="contactusButton"  value="Submit Comment" />
                        </div>
                   </form>
                </div>
                    {/* ----------------------- */}
                    <div className="contactDetail">
                        <h3>Reservation</h3>
                        <div className="reservationbottomline"></div>
                        <div className="container">
                                <form action="/action_page.php" onSubmit={handleHotelForm}>
                                <div className="row">
                                    <div className="col-75">
                                    <div>{ checkingIn ? <CheckInCalender onChange={handelGetCheckingIn} className="CheckInCalenderHotelFacility" /> : null}</div>
                                        <input type="text"  value={getCheckInDate}  className="firstInput" id="fname" name="firstname" placeholder="Check In" onClick={handleCheckInOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                    <div>{ checkingOut ? <CheckOutCalender  onChange={handleGetCheckingOut} className="CheckOutCalenderHotelFacility" /> : null  }</div>
                                        <input type="text" value={getCheckOutDate} id="lname" className="secondInput" name="lastname" placeholder="Check Out" onClick={handleCheckOutOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="firstSelect">
                                        <option value="Rooms">Rooms</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="secondSelect">
                                        <option value="persons">Persons</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <input type="submit" name="submit" className="submittypebutton" value="Check Availability" />
                                </div>
                            </form>
                        </div>
                            <div className="silverLine"></div>
                        {/* ----------------------- */}
                        <div className="ContactForm">
                            <h3>Contact Detail</h3>
                            <div className="reservationbottomline"></div>
                            <div className="makingDown">
                                <div className="contact">
                                    <PhoneIcon />
                                    <p className="setp">+21345612345</p>
                                </div>
                                <div className="contact">
                                    <SubjectIcon />
                                    <p className="setp">123454234</p>
                                </div>
                                <div className="contact">
                                    <EmailIcon />
                                    <p className="setp">contactus@eurostarinn.com</p>
                                </div>
                                <div className="contact">
                                    <HomeIcon />
                                    <p className="setp">Khajuraho</p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            {/* ----------------- */}
        </div>
    )
}

export default ContactUs
