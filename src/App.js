import React from 'react';
import './App.css';
import Navigation from "./Navigation";
import Container from '@material-ui/core/Container';
import Home from "./Home";
import Footer from "./Footer";
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import Booking from "./Booking";
import HotelFacility from "./HotelFacility";
import PhotoGallery from "./PhotoGallery";
import Accomodation from "./Accomodation";
import Contact from "./ContactUs";
import ExecutiveRoom from "./ExecutiveRoom";
import DeluxeRoom from "./DeluxeRoom";
import StandardRoom from "./StandardRoom"
import LuxuryRoom from "./LuxuryRoom";
import SpecialOffer from "./SpecialOffer";


function App() {
  return (
    <Router>
    <div >
      <Container maxWidth="lg" className="App">
        <Navigation />
        <Switch>
          <Route path="/PhotoGallery"><PhotoGallery /></Route>
          <Route path="/Booking"><Booking /></Route>
          <Route path="/HotelFacility"><HotelFacility /></Route>
          <Route path="/Accomodation"><Accomodation/></Route>
          <Route path="/ContactUs"><Contact /></Route>
          <Route path="/ExecutiveRoom"><ExecutiveRoom /></Route>
          <Route path="/DeluxeRoom"><DeluxeRoom /></Route>
          <Route path="/StandardRoom"><StandardRoom /></Route>
          <Route path="/LuxuryRoom"><LuxuryRoom /></Route>
          <Route path="/SpecialOffer"><SpecialOffer /></Route>
          <Route path="/">
            <Home /> 
          </Route>
        
        </Switch>
        <Footer />
      </Container>
    </div>
    </Router>
  );
}

export default App;
