import React from 'react'
import "./Booking.css"

function Booking() {

    function handleReservationSubmit(event) {
        event.preventDefault();
    }

    return (
        <div className="booking">
            <div className="booking_header_image"></div>
            <div className="booking_h3"><h3>Reservation : Your Date</h3></div>
            <div className="Reservation_confirmation">
                <div className="reservation_name">
                    <h4>Your Reservation</h4>
                </div>
                <div className="bottom_redline"></div>
                <form className="reservation-form" onSubmit={handleReservationSubmit}>
                    <div className="room-type">
                        <label>Room Type</label>
                        <select className="roomtype-select">
                            <option value="chooseroomtype">Choose room type</option>
                            <option value="executive">EXECUTIVE</option>
                            <option value="dulux">DULUX</option>
                            <option value="luxury">LUXURY</option>
                            <option value="standard">STANDARD</option>
                        </select>
                    </div>
                        <div className="silverLine"></div>
                    <div className="checkIn-Out">
                        <div className="checkin">
                            <label>Check In</label>
                            <input type="text" name="checkin" className="checkInInput" placeholder="Check In" />
                        </div>
                        <div className="checkout">
                            <label>Check Out</label>
                            <input type="text" name="checkout" className="checkOutInput" placeholder="Check Out" />
                        </div>
                    </div>
                    <div className="silverLine"></div>
                    <div clasName="roomspersons  RoomsPerson">
                        <div className="noofrooms">
                            <label>No. Of Rooms</label>
                            <input type="text" name="rooms" placeholder="Rooms" className="roominput" />
                        </div>
                        <div className="noofpersons">
                            <label>No. of Persons</label>
                            <input type="text" name="persons" placeholder="Persons" className="personsinput" />
                        </div>

                    </div>
                        <div className="silverLine"></div>
                    <div className="form-Name">
                        <label>Name </label>
                        <input type="text" name="name" placeholder="Name" className="nameInput" />
                    </div>
                        <div className="silverLine"></div>
                    <div className="form-mobile">
                        <label>Mobile</label>
                        <input type="text" name="mobile" placeholder="Mobile" className="mobileInput" />
                    </div>
                        <div className="silverLine"></div>
                    <div className="form-email">
                        <label>Email</label>
                        <input type="text" name="email" placeholder="E-mail" className="emailInput" />
                    </div>
                        <div className="silverLine"></div>
                    <div className="form-button">
                        <input type="submit" name="submit"  value="Check Availability" className="inputTypeSubmit" />
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Booking
