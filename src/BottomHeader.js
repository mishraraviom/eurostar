import React,{useState} from 'react';
import "./BottomHeader.css";
import BottomHeaderTwo from "./BottomHeader_2";
import {Link} from "react-router-dom";

function BottomHeader() {

    const [Header,setHeader]=useState(false);

    const [addClass , setAddClass] =useState(false);

    const showHeader = () => {
        if(window.innerWidth <= 1050){
            setHeader(true);
        }else{
            setHeader(false);
        }
    }

    window.addEventListener('resize',showHeader);

    function myFunction() {
        setAddClass(true);
      }
    
    function MyFunction(){
        setAddClass(false);
    }

    return (
        <div>
        {Header ? <BottomHeaderTwo /> : 

        <div className="bottomheader">
            <div className="bottomheader_image">
                <Link className="link" to="/" style={{textDecoration: "none"}}>
                    <img  src={require("./images/esi_logo.gif")} alt="" className="bottomHeaderImage" />
                </Link>
            </div>
            <div className="bottomheader_navbar">
                <div className="navbar_first navbarfirst_home">
                    <Link className="link" to="/" style={{textDecoration: "none"}}>
                        <h5 style={{color : "black"}}>HOME</h5>
                        <p style={{color:"#B3C5C9"}}>Welcome</p>
                    </Link>
                </div>
                <div className="bottomheader_sideline"></div>
                <div className="navbar_second" onMouseOver={myFunction} onMouseOut={MyFunction}>
                    <Link to="/Accomodation" className="link" style={{textDecoration: "none" , color: "black"}}><h5 >ACCOMODATION</h5></Link>
                    <p>Room Categories & specification</p>
                    <div id="myDropdown" className={addClass ? "dropdown-content show" : "dropdown-content"}>
                        <Link to="/Booking" className="a link" style={{textDecoration: "none"}}>Booking</Link>
                        <Link to="/HotelFacility" className="a link" style={{textDecoration: "none"}}>HotelFacility</Link>
                        <Link to="/PhotoGallery" className="a link" style={{textDecoration: "none"}}>PhotoGallery</Link>
                    </div>
                </div>
                <div className="bottomheader_sideline"></div>
                <div className="navbar_third">
                    <a href="https://www.tripadvisor.in/Hotel_Review-g297647-d2041217-Reviews-EuroStar_Inn-Khajuraho_Chhatarpur_District_Madhya_Pradesh.html" style={{textDecoration:"none",color : "black"}}><h5>REVIEW US</h5></a>
                    <p>Share your love withUs</p>
                </div>
                <div className="bottomheader_sideline"></div>
                <div className="navbar_fourth">
                    <Link className="link" to="/ContactUs" style={{textDecoration: "none"}}>
                        <h5 style={{color:"black"}}>Contact Us</h5>
                        <p style={{color : "#B3C5C9"}}>Get in touch</p>
                    </Link>
                </div>
            </div>
        </div>
        }
        </div>
    )
}

export default BottomHeader
