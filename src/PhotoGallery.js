import React from 'react'
import "./PhotoGallery.css"
import Images from "./Images";
                 
function PhotoGallery() {
    return (
        <div className="photogallery" >
            <div className="photogalleryimage"></div>
            <div className="photogallery_h" ><h4>Photo Gallery</h4></div>
            <div className="configuring">
                <div className="photogallery-images">
                    <div className="photogalleryImages">
                        <Images img={require("./images/Deluxe(1).jpg")} />
                        <Images img={require("./images/Deluxe(2).jpg")} />
                        <Images img={require("./images/Deluxe(3).jpg")} />
                    </div>
                    <div className="photogalleryImages">
                        <Images img={require("./images/Deluxe(4).jpg")} />
                        <Images img={require("./images/Deluxe(5).jpg")} />
                        <Images img={require("./images/DEluxe(6).jpg")} />
                    </div>    
                    <div className="photogalleryImages">
                        <Images img={require("./images/Deluxe(7).jpg")} />
                        <Images img={require("./images/Executive.jpg")} />
                        <Images img={require("./images/Executive(1).jpg")} />
                    </div>
                    <div className="photogalleryImages">
                        <Images img={require("./images/Executive(2).jpg")} />
                        <Images img={require("./images/Executive(3).jpg")} />
                        <Images img={require("./images/Executive(4).jpg")} />
                    </div>      
                    <div className="photogalleryImages">
                        <Images img={require("./images/Executive(5).jpg")} />
                        <Images img={require("./images/Executive(6).jpg")} />
                        <Images img={require("./images/hotel_front_view.jpg")} />
                    </div>
                </div>                
            </div>
        </div>
    )
}

export default PhotoGallery
