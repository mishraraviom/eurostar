import React from 'react'
import "./AccomodationCard.css"
import AccomodationCard from "./AccomodationCard";

function Accomodation() {

    return (
        <div className="AccomodationCard">
            <div className="accomodation_header_Image"></div>
            <div className="accomodation_h3"><h3>Accomodation</h3></div>
            <div className="AccomodationCard-aligned">
                <AccomodationCard 
                    img={require("./images/Deluxe.jpg")}
                    heading={"Executive Rooms"}
                    Bedsize={"King"}
                    occupancy={"2 persons"}
                    Balcony={"Yes"}
                    AC={"yes"}
                    link1={"/ExecutiveRoom"}
                    link2={"/Booking"}
                />
                <AccomodationCard 
                    img={require("./images/Deluxe.jpg")}
                    heading={"Deluxe Rooms"}
                    Bedsize={"King"}
                    occupancy={"2 persons"}
                    Balcony={"No"}
                    AC={"yes"}
                    link1={"/DeluxeRoom"}
                    link2={"/Booking"}
                />
                <AccomodationCard 
                    img={require("./images/Deluxe.jpg")}
                    heading={"Luxury Rooms"}
                    Bedsize={"Double/Twin"}
                    occupancy={"2 persons"}
                    Balcony={"No"}
                    AC={"yes"}
                    link1={"/LuxuryRoom"}
                    link2={"/Booking"}
                />
            </div>
            <div className="AccomodationCard-aligned">
                <AccomodationCard 
                    img={require("./images/Deluxe.jpg")}
                    heading={"Standard Rooms"}
                    Bedsize={"Double/Twin"}
                    occupancy={"2 persons"}
                    Balcony={"No"}
                    AC={"No"}
                    link1={"/StandardRoom"}
                    link2={"/Booking"}
                />
            </div>
        </div>
    );
}

export default Accomodation

