import React,{useState} from 'react';
import "./StandardRoom.css";
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import SubjectIcon from '@material-ui/icons/Subject';
import HomeIcon from '@material-ui/icons/Home';
import CheckInCalender from "react-calendar";
import CheckOutCalender from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import CheckIcon from '@material-ui/icons/Check';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Carousel} from "react-bootstrap";

function StandardRoom() {

    const [showDialog,setShowDialog] = useState(true);
    const [facilityShowDialog,setFacilityShowDialog] = useState(false);



    function handleShowDialogs(event){
        const getButtonName = event.target.value;

        if(getButtonName==="button1"){
            setShowDialog(true);
            setFacilityShowDialog(false);
        }else if(getButtonName==="button2"){
            setShowDialog(false);
            setFacilityShowDialog(true);
        }
    }

    

     // /////////////////////////////

     const [checkingIn,setCheckingIn] = useState(false);
     const [checkingOut,setCheckingOut] = useState(false);
     const [getCheckInDate,setCheckInDate] =useState(checkingIn ? new Date() : null);
     const [getCheckOutDate,setCheckOutDate]=useState(checkingOut ? new Date() : null);
 
 
     function handelGetCheckingIn(ranges){
         setCheckInDate(ranges.toLocaleDateString());
     }
 
     function handleGetCheckingOut(ranges){
         setCheckOutDate(ranges.toLocaleDateString());
     }
 
     function handleCheckInOnClick(){
         setCheckingIn(prevValue=>{
             return !prevValue;
         });
     }
 
     function handleCheckOutOnClick(){
         setCheckingOut(prevValue=>{
             return !prevValue;
         });
     }
 
     //////////////////////////////
 
     function handleHotelForm(event){
         event.preventDefault();
     }

     /////////carousal/////////
     const [index, setIndex] = useState(0);

  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
     /////////////////////////
    return (
        <div>
            <div className="Standard_header_Image"></div>
            <div className="Standard_h3"><h3>Standard Rooms</h3></div>
            {/* ----------- */}

            <div className="Standard_bodypart">
                <div className="Standard_portion">
                {/* -------------------------- */}
                <Carousel activeIndex={index} onSelect={handleSelect} className="Standard-carousel-theme">
        <Carousel.Item>
          <img
            className="d-block w-100 executiveImageView"
            src={require("./images/hotel_front_view.jpg")}
            alt="First slide"
          />
          <Carousel.Caption>
            {/* <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 executiveImageView"
            src={require("./images/reception_seating_area.jpg")}
            alt="Second slide"
          />
  
          <Carousel.Caption>
            {/* <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> */}
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 executiveImageView"
            src={require("./images/restaurent1.jpg")}
            alt="Third slide"
          />
  
          <Carousel.Caption>
            {/* <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p> */}
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
                {/* -------------------------- */}
                    <div className="Standard_room_overview">
                        <div>
                            <button className="overviewButton1" onClick={handleShowDialogs} value="button1">Room OverView</button>
                            <div className={showDialog ? "Deluxe-overview-info showdialog" : "Deluxe-overview-info "}>
                                <div className="info-part1">
                                    <div className="info-Para infoParaBack">
                                        <p><span className="executive-span ">Bed :</span> King</p>
                                    </div>
                                    <div className="info-Para ">
                                        <p><span className="executive-span">Occupancy :</span> 2 Persons</p>
                                    </div>
                                    <div className="info-Para infoParaBack">
                                        <p><span className="executive-span infoPara">Ensuite Bathroom :</span> Yes</p>
                                    </div>
                                    <div className="info-Para ">
                                        <p><span className="executive-span ">Air Condition :</span> No</p>
                                    </div>
                                </div>
                                <div className="info-part2">
                                    <div className="info-Para" >
                                        <p><span className="executive-span infoPara">Free wifi :</span> No</p>
                                    </div>
                                    <div className="info-Para infoParaBack">
                                        <p><span className="executive-span ">Breakfast Include :</span> No</p>
                                    </div>
                                    <div className="info-Para">
                                        <p><span className="executive-span infoPara">Ensuite Balcony :</span> No</p>
                                    </div>
                                    <div className="info-Para infoParaBack">
                                        <p><span className="executive-span">Room Service :</span> Yes (24/7)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button className="overviewButton2" onClick={handleShowDialogs} value="button2">Hotel Facility</button>
                            <div className={ facilityShowDialog?"facility-info facilityToggle": "facility-info"}>
                                <div className="facility-info-part1">
                                    <div className="facility-para para-background">
                                        <CheckIcon />
                                        <p className="p">24 Hours Room Service</p>
                                    </div>
                                    <div className="facility-para ">
                                        <CheckIcon />
                                        <p className="p">Air Conditioned Rooms</p>
                                    </div>
                                    <div className="facility-para para-background">
                                        <CheckIcon />
                                        <p className="p">Cable LCD T.V</p>
                                    </div>
                                    <div className="facility-para ">
                                        <CheckIcon />   
                                        <p className="p">Wake up call services</p>
                                    </div>
                                    <div className="facility-para para-background">
                                        <CheckIcon />
                                        <p className="p">Hot water 24/7</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    {/* ----------------------- */}
                    <div className="contactDetail">
                        <h3>Reservation</h3>
                        <div className="reservationbottomline"></div>
                        <div className="container">
                                <form action="/action_page.php" onSubmit={handleHotelForm}>
                                <div className="row">
                                    <div className="col-75">
                                         <div>{ checkingIn ? <CheckInCalender onChange={handelGetCheckingIn} className="CheckInCalenderHotelFacility" /> : null}</div>
                                        <input type="text" value={getCheckInDate} className="firstInput" id="fname" name="firstname" placeholder="Check In" onClick={handleCheckInOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <div>{ checkingOut ? <CheckOutCalender  onChange={handleGetCheckingOut} className="CheckOutCalenderHotelFacility" /> : null  }</div>
                                        <input type="text" value={getCheckOutDate} id="lname" className="secondInput" name="lastname" placeholder="Check Out"  onClick={handleCheckOutOnClick} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="firstSelect">
                                        <option value="Rooms">Rooms</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-75">
                                        <select id="country" name="country" className="secondSelect">
                                        <option value="persons">Persons</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <input type="submit" name="submit" className="submittypebutton" value="Check Availability" />
                                </div>
                            </form>
                        </div>
                            <div className="silverLine"></div>
                        {/* ----------------------- */}
                        <div className="ContactForm">
                            <h3>Contact Detail</h3>
                            <div className="reservationbottomline"></div>
                            <div className="makingDown">
                                <div className="contact">
                                    <PhoneIcon />
                                    <p className="setp">+21345612345</p>
                                </div>
                                <div className="contact">
                                    <SubjectIcon />
                                    <p className="setp">123454234</p>
                                </div>
                                <div className="contact">
                                    <EmailIcon />
                                    <p className="setp">contactus@eurostarinn.com</p>
                                </div>
                                <div className="contact">
                                    <HomeIcon />
                                    <p className="setp">Khajuraho</p>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>

            {/* ----------- */}
        </div>
    )
}

export default StandardRoom
