import React from 'react'
import "./Images.css";

function Images(props) {
    return (
        <div className="images">
            <img src={props.img} className="pics" alt="" />
        </div>
    )
}

export default Images
