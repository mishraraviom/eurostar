import React , {useState} from 'react'
import "./TopHeader.css"
import PhoneIcon from '@material-ui/icons/Phone';
import MailIcon from '@material-ui/icons/Mail';
import {Link} from "react-router-dom";

function TopHeader() {

    const [length,setLength]=useState(false);

    const SetSize=()=> {if(window.innerWidth <= 766){
        setLength(true);
    }else{
        setLength(false);
    }}

    window.addEventListener('resize',SetSize);

    return (
        <div className="topheader">
            {length ? 
            <div className="topheader__contactdetail_button">
                <PhoneIcon />
                <Link to="/Booking"><button className="buttonVariant">Book Now</button></Link>
                <MailIcon />
            </div> :
            <div className="topheader__contactdetail">
                <PhoneIcon />
                <p>+919981296029</p>
                <MailIcon />
                <p>contactus@eurostarinn.com</p>
                <Link to="/Booking"><button className="buttonVariant2">Book Now</button></Link>
            </div>}
                
        </div>
    )
}

export default TopHeader
