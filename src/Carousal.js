import React , {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Carousel} from "react-bootstrap";
import "./Carousal.css";

function Carousal() {
    const [index, setIndex] = useState(0);

  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
    return (
        <div className="carousalItems">
            <Carousel activeIndex={index} onSelect={handleSelect} className="carousel_theme">
        <Carousel.Item>
          <img
            className="d-block w-100 imageView"
            src={require("./images/hotel_front_view.jpg")}
            alt="First slide"
          />
          <Carousel.Caption>
            {/* <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 imageView"
            src={require("./images/reception_seating_area.jpg")}
            alt="Second slide"
          />
  
          <Carousel.Caption>
            {/* <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> */}
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 imageView"
            src={require("./images/restaurent1.jpg")}
            alt="Third slide"
          />
  
          <Carousel.Caption>
            {/* <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p> */}
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
        </div>
    )
}

export default Carousal
